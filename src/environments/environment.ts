// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBl91_NWp1WnPM62Oesgp0rK0goPodTQd0",
    authDomain: "i18n-c3367.firebaseapp.com",
    databaseURL: "https://i18n-c3367.firebaseio.com",
    projectId: "i18n-c3367",
    storageBucket: "i18n-c3367.appspot.com",
    messagingSenderId: "758162422710",
    appId: "1:758162422710:web:eb8e2823f7fc6046cc1f17",
    measurementId: "G-0N7V0NYYW1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
