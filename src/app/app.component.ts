import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../projects/i18n-translate/src/lib/services/language.service';
import {LanguageModel} from '../../projects/i18n-translate/src/lib/models/language.model';
import {FirebaseService} from '../../projects/i18n-translate/src/lib/services/firebase.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  languages: LanguageModel[];
  activeLanguageId: string;
  constructor(private languageService: LanguageService, private _firebaseService: FirebaseService) {}
  ngOnInit(): void {
    this.languageService.getLanguages().subscribe(data => {
      this.languages = this._firebaseService.firebaseNormalizedData(data);
      this.activeLanguageId = null;
      this.activeLanguageId = this.languages.find(l => l.active)?.id;
      console.log('YAY');
    });
  }

  setActiveLang() {
    const lang: LanguageModel = this.languages.find(l => l.id === this.activeLanguageId);
    this.languageService.setActiveLang(lang);
  }
}
