import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {I18nTranslateModule} from '../../projects/i18n-translate/src/lib/i18n-translate.module';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFirestore} from '@angular/fire/firestore';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [AppComponent],
    imports: [BrowserModule, I18nTranslateModule, AngularFireModule.initializeApp(environment.firebaseConfig), AngularFireDatabaseModule, FormsModule],
  providers: [AngularFirestore],
  bootstrap: [AppComponent],
})
export class AppModule {}
