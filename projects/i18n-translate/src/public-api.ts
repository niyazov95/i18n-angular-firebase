/*
 * Public API Surface of i18n-translate
 */

export * from './lib/services/i18n-translate.service';
export * from './lib/components/i18n-translate.component';
export * from './lib/i18n-translate.module';
