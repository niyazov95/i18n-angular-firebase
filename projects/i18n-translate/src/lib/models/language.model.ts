export class LanguageModel {
  id?: string;
  name: string;
  default?: boolean;
  active?: boolean;
}
