import {TranslationsModel} from "./translations.model";

export class WordModel {
  id?: string;
  key: string;
  translation: TranslationsModel[]
}

export interface ISingleWord {
  id?: string;
  value: string;
}
