import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {LanguageService} from '../../services/language.service';
import {LanguageModel} from '../../models/language.model';
import {FirebaseService} from '../../services/firebase.service';
import {ISingleWord, WordModel} from '../../models/word.model';
import {I18nTranslateService} from '../../services/i18n-translate.service';
import {TranslationsModel} from '../../models/translations.model';

@Component({
  selector: 'i18n-modal',
  template: `
    <div class="modal-backdrop"></div>
    <div class="modal">
      <header class="modal-header">
        <h3>Keyword: {{ word?.key }}</h3>
        <a (click)="handleClose.emit()">&times;</a>
      </header>
      <main class="modal-content">
        <div class="language">
          <div class="language-list">
            <select [(ngModel)]="selectedLangId" (ngModelChange)="handleSelectLang()">
              <option disabled [value]="undefined">Select Language</option>
              <option *ngFor="let lang of languages" [value]="lang.id">{{ lang.name }}</option>
            </select>
          </div>
          <div class="language-control">
            <input [(ngModel)]="languageControl" type="text" placeholder="type language name" />
          </div>
          <div class="language-action">
            <button (click)="createLang()">Add Language</button>
            <button [disabled]="!selectedLangId" (click)="deleteLang(selectedLang)">Delete Language</button>
          </div>
        </div>
        <div class="translation">
          <div class="translation-control">
            <input
              [disabled]="!selectedLang"
              type="text"
              [(ngModel)]="translationControl"
              (ngModelChange)="handleAddTranslation()"
              placeholder="Enter {{ selectedLang?.name }} translation"
            />
          </div>
          <div class="translation-action">
            <button [disabled]="!selectedLang" (click)="handleUpdateWord()">Save</button>
          </div>
        </div>
      </main>
      <!--      <footer class="modal-footer">-->
      <!--        <button>Save</button>-->
      <!--        <button>Close</button>-->
      <!--      </footer>-->
    </div>
  `,
  styleUrls: ['./i18n-modal.component.scss'],
})
export class I18nModalComponent implements OnChanges {
  @Output() handleClose: EventEmitter<any> = new EventEmitter<any>();
  @Input() wordId: string;
  word: WordModel;
  translationControl: string;
  translations: TranslationsModel[] = [];

  languageControl: string;
  selectedLangId: string;
  selectedLang: LanguageModel;
  languages: LanguageModel[];
  constructor(private _languageService: LanguageService, private _firebaseService: FirebaseService, private _i18nService: I18nTranslateService) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.word = this._i18nService.getWordById(this.wordId);
    console.log(this.word);
    this._languageService.getLanguages().subscribe(languages => {
      this.languages = this._firebaseService.firebaseNormalizedData(languages);
      this.languages.forEach(l => {
        if (!this.word.translation.length) {
          const translation: TranslationsModel = {
            langId: l.id,
            value: this.word.key,
          };
          this.translations.push(translation);
        } else {
          this.translations = this.word.translation;
        }
      });
    });
  }

  createLang() {
    const dto = {
      name: this.languageControl,
    };
    this._languageService.createLang(dto).then(() => {
      console.log('Created');
      this.languageControl = '';
    });
  }
  deleteLang(lang: LanguageModel) {
    this._languageService.deleteLang(lang);
    this.selectedLang = null;
    this.selectedLangId = null;
  }

  handleSelectLang() {
    this.selectedLang = this.languages.find(l => l.id === this.selectedLangId);
    this.translationControl = this.translations.find(t => t.langId === this.selectedLangId).value;
  }

  handleUpdateWord() {
    const dto: WordModel = {
      id: this.word.id,
      key: this.word.key,
      translation: this.translations,
    };
    this._i18nService.updateWord(dto);
    this.translationControl = '';
  }

  handleAddTranslation() {
    this.translations.find(t => t.langId === this.selectedLangId).value = this.translationControl;
  }
}
