import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ISingleWord} from '../../models/word.model';

@Component({
  selector: 'i18n-tooltip',
  template: `
    <div class="i18n-tooltip">
      <div class="i18n-tooltip__text">
        <em>{{ singleWord.value }}</em>
      </div>
      <div class="i18n-tooltip__action">
        <button (click)="$handleAction.emit(singleWord.id ? 'edit' : 'add')" [title]="singleWord.id ? 'Edit' : 'Add'">
          {{ singleWord.id ? '&#9998;' : '&#43;' }}
        </button>
        <button (click)="$closeTooltip.emit()" title="Close">&times;</button>
      </div>
    </div>
  `,
  styleUrls: ['./i18n-tooltip.component.scss'],
})
export class I18nTooltipComponent implements OnInit {
  @Input() public singleWord: ISingleWord;
  @Output() $closeTooltip: EventEmitter<any> = new EventEmitter<any>();
  @Output() $handleAction: EventEmitter<'edit' | 'add'> = new EventEmitter<'edit' | 'add'>();

  constructor() {}

  ngOnInit(): void {}
}
