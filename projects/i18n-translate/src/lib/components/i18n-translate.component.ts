import {Component, Input, OnInit} from '@angular/core';
import {I18nTranslateService} from '../services/i18n-translate.service';
import {ISingleWord, WordModel} from '../models/word.model';

@Component({
  selector: 'i18n-translate',
  template: `
    <ng-container>
      <span class="i18n" (mouseenter)="handleTooltip()">
        {{ getWord().value }}
        <span class="i18n-tooltip" *ngIf="tooltipIsOpen">
          <i18n-tooltip ($handleAction)="handleAction($event)" ($closeTooltip)="tooltipIsOpen = false" [singleWord]="getWord()"></i18n-tooltip>
        </span>
        <span class="i18n-modal" *ngIf="modalIsOpen">
          <i18n-modal (handleClose)="modalIsOpen = false" [wordId]="getWord().id"></i18n-modal>
        </span>
      </span>
    </ng-container>
  `,
  styleUrls: ['./i18n-translate.component.scss'],
})
export class I18nTranslateComponent implements OnInit {
  @Input() private key: string;

  public tooltipIsOpen: boolean;
  public modalIsOpen: boolean;

  constructor(private _i18nService: I18nTranslateService) {}

  ngOnInit(): void {}

  handleAction(event: 'edit' | 'add') {
    event === 'add' ? this.createWord() : this.editWord();
  }

  createWord() {
    const dto: WordModel = {
      key: this.key,
      translation: [],
    };
    this._i18nService.createWord(dto).then(() => console.log('created'));
  }
  editWord() {
    this.modalIsOpen = true;
  }

  public getWord(): ISingleWord {
    return this._i18nService.getWord(this.key);
  }

  handleTooltip() {
    this.tooltipIsOpen = true;
    setTimeout(() => (this.tooltipIsOpen = false), 5000);
  }
}
