import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  constructor(private firestore: AngularFirestore) {}

  public firebaseNormalizedData(data) {
    return data.map(e => {
      return {
        id: e.payload.doc.id,
        ...e.payload.doc.data(),
      };
    });
  }

  public get(url: string) {
    return this.firestore.collection(url).snapshotChanges();
  }
  public create(url: string, dto: any) {
    console.log(dto)
    return this.firestore.collection(url).add(dto);
  }
  public update(url: string, dto: any) {
    // delete dto.id;
    this.firestore.doc(`${url}/${dto.id}`).update(dto);
  }
  public delete(url: string, dto: any) {
    this.firestore.doc(`${url}/${dto.id}`).delete();
  }
}
