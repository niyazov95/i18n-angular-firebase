import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {LanguageModel} from '../models/language.model';

@Injectable({
  providedIn: 'root',
})
export class LanguageService {
  private url = '/languages';
  languages: LanguageModel[];
  activeLang: LanguageModel;

  constructor(private _firebaseService: FirebaseService) {
    this.getLanguages().subscribe(data => {
      this.languages = this._firebaseService.firebaseNormalizedData(data);
      // this.setActiveLang(this.languages.find(l => l.active));
      if (!this.activeLang) {
        this.setActiveLang(this.languages[0]);
      }
    });
  }

  get getActiveLang(): LanguageModel {
    return this.activeLang;
  }

  setActiveLang(lang) {
    this.activeLang = lang;
    this.languages.forEach(l => {
      if (lang.id === l.id) {
        l.active = true;
        this.updateLang(l);
      } else {
        l.active = false;
        this.updateLang(l);
      }
    });
  }

  // CRUD
  getLanguages() {
    return this._firebaseService.get(this.url);
  }
  createLang(dto: LanguageModel) {
    return this._firebaseService.create(this.url, dto);
  }
  updateLang(dto: LanguageModel) {
    this._firebaseService.update(this.url, dto);
  }
  deleteLang(dto: LanguageModel) {
    this._firebaseService.delete(this.url, dto);
  }
}
