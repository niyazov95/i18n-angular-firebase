import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {ISingleWord, WordModel} from '../models/word.model';
import {LanguageService} from './language.service';

@Injectable({
  providedIn: 'root',
})
export class I18nTranslateService {
  private url = '/words';
  public words: WordModel[];

  constructor(private _firebaseService: FirebaseService, private _languageService: LanguageService) {
    this.getWords().subscribe(data => {
      this.words = this._firebaseService.firebaseNormalizedData(data);
    });
  }

  // CRUD
  getWords() {
    return this._firebaseService.get(this.url);
  }
  getWordById(id: string): WordModel {
    return this.words?.find(w => w.id === id);
  }
  getWord(key: string): ISingleWord {
    let w = {
      id: '',
      value: '',
    };
    const word = this.words?.find(w => w.key === key);
    if (word) {
      const currentLangWord = word.translation.find(w => w.langId === this._languageService.getActiveLang?.id);
      if (currentLangWord) {
        w.id = word.id;
        w.value = currentLangWord.value;
        return w;
      } else {
        w.id = word.id;
        w.value = key;
        return w;
      }
    } else {
      w.value = key;
      return w;
    }
    // return w;
  }
  createWord(dto: WordModel) {
    return this._firebaseService.create(this.url, dto);
  }
  updateWord(dto: WordModel) {
    this._firebaseService.update(this.url, dto);
  }
  deleteWord(dto: WordModel) {
    this._firebaseService.delete(this.url, dto);
  }
}
