import {NgModule} from '@angular/core';
import {I18nTooltipComponent} from './components/i18n-tooltip/i18n-tooltip.component';
import {I18nTranslateComponent} from './components/i18n-translate.component';
import {CommonModule} from '@angular/common';
import {I18nModalComponent} from './components/i18n-modal/i18n-modal.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [I18nTranslateComponent, I18nTooltipComponent, I18nModalComponent],
  imports: [CommonModule, FormsModule],
  providers: [],
  exports: [I18nTranslateComponent],
})
export class I18nTranslateModule {}
